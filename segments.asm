	;; segments.asm

	;; Copyright (C) 2018, J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; segment and TSS info and addresses
	;; see gdt.asm for more information

%ifndef HEADER_SEGMENTS
%define HEADER_SEGMENTS

CS_KERN		equ	0x08
SS_KERN_PROC	equ	0x10
SS_KERN_CORE	equ	0x28
CS_USER		equ	0x1b
SS_USER		equ	0x23

extern gdt
extern isp0
extern ist0

	;; KSP stands for Kernel Stack Pointer (address)

section .rodata
KSP_PROC	equ	0xffffff0000015104
KSP_CORE	equ	0xffffff0000015104

kspProc:dq KSP_PROC
kspCore:dq KSP_CORE

%endif HEADER_SEGMENTS
