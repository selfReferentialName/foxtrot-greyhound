	;; io.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; handles input and output

section .rodata

ioperms:
	dq 0xffffff0000016000
getcr3:	dq 0xffffe00000000068

x86_ops:
.out:	db 0x90					; one byte NOP
	out dx, al
	out dx, ax
	out dx, eax
.in:	db 0x90					; one byte NOP
	in al, dx
	in ax, dx
	in eax, dx

section .text
global sysDropIo
global sysShareIo
global sysIo
extern scJump
extern scError
extern setPfFail
extern unsetPf

checkPriv:					; check i/o privilages
	push rax
	push rdx
	push rcx
	and rdx, 0xffff
	shr dx, 3				; get the offset in the i/o privilage bitmap
	add rdx, [ioperms]
	mov al, [rdx]				; get the byte the permission bit is in
	mov cl, [rsp]
	and cl, 0x07				; get the offset in the byte
	mov ah, 1
	shl ah, cl				; select the bit to test
	test al, ah
	jnz checkPriv.error			; if the bit isn't unset (is set), the operation is illegal
	pop rcx
	pop rdx
	pop rax
	ret					; else, it is legal
.error:	pop rcx
	pop rdx
	pop rax
	pop rax					; trash the return address, we won't use it
	pop r11
	pop rcx
	mov rax, 1
	jmp scError

sysDropIo:
	push rcx
	mov cl, dl
	and cl, 0x07				; get the offset in the byte
	mov ah, 1
	shl ah, cl				; select the bit to set
	pop rcx
	and rdx, 0xffff
	shr dx, 3				; get the offset in the i/o privilage bitmap
	add rdx, [ioperms]
	or [rdx], ah				; set the bit
	jmp scJump

sysShareIo:
	call setPfFail
	sub rsp, 4 * 8				; make this look like it's in xio so that checkPriv can fail correctly
	call checkPriv
	add rsp, 4 * 8
	mov edi, edi				; clear the high 32 bits of rdi
	shl rdi, 12				; make it a page selecter
	add rdi, [getcr3]
	mov rax, [rdi]	; load the cr3 of the target process
	cli					; process switchy stuff
	mov rsi, cr3				; save our cr3
	mov cr3, rax				; load the new cr3
	push rcx
	mov cl, dl
	and cl, 0x07				; offset in the byte
	mov ah, 0xfe				; not one
	shl ah, cl				; select the bit to set
	pop rcx
	and rdx, 0xffff
	shr dx, 3
	mov rdi, [ioperms]
	invlpg [rdi]				; sanity is good
	and [rdx + rdi], ah			; unset the bit
	mov cr3, rsi				; come back to the process' address space
	invlpg [rdi]
	sti
	call unsetPf
	jmp scJump

	; TODO TODO TODO TODO RIGHT NOW: make raw io syscalls
sysIo:
