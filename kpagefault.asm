	;; kpagefault.asm

	;; Copyright (C) 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; handle a page fault within the kernel because of a missing PID or bad access to a user page

%include "segments.asm"

section .rodata

ssOORsp:dq 0xffffff00000132a8
proclist:
	dq 0xffffe00000000000
kstart:	dq 0xffffff0000000000

section .text
global pfFail				; fail on any page fault (differing error codes by location) (requires rcx and r11 to be usable for sysret)
global pfCow				; TODO handle a COW page
global pfPage				; TODO handle higher level paging creation
global setPfFail			; set #PF as pfFail in the IDT
global setPfCow
global setPfPage
global unsetPf				; set the #PF handler back to the process' #PF
extern scError
extern idtEntry

pfCow:
pfPage:
	; BOTH OF THESE ARE VERY TODO TODO TODO TODO

setPfFail:
	push rax
	mov rax, pfFail
	call setPf
	pop rax
	ret

setPfCow:
	push rax
	mov rax, pfCow
	call setPf
	pop rax
	ret

setPfPage:
	push rax
	mov rax, pfPage
	call setPf
	pop rax
	ret

unsetPf:
	push rax
	push rdx
	push rdi
	mov rax, [0x2038]
	mov dx, 0x0eef
	mov di, CS_USER
	call idtEntry
	pop rdi
	pop rdx
	pop rax
	ret

setPf:	push rdx			; general #PF setter
	push rdi
	mov dx, 0x0e8f			; #PF, DPL 0, trap
	mov di, CS_KERN
	call idtEntry
	pop rdi
	pop rdx
	ret

pfFail:	mov rsp, [ssOORsp]
	mov rsp, [rsp]
	sub rsp, 8
	mov rdx, cr2			; linear faulting address
	mov rax, 4			; bad user page error
	cmp rdx, [proclist]
	jl scError
	mov al, 2
	cmp rdx, [kstart]
	jl scError
	mov al, 4
	cmp rdx, [kstart]
	jl scError
	mov al, 3
	cmp rdx, [kstart]
	jl scError
	mov al, 4
	jmp scError
