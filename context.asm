	;; context.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; an object to handle context switching
	;; (due to battson this includes rpc)
	;; to save on code size, this also provides routines often used in context switches
	;; i.e. rtuspc (return to the user space without a stack set up for iretq)
	;; and saveState (save the processes state after an interrupt)

%include "segments.asm"

global pidCr3				; start of the process list and the lower 12 bits to get Cr3
global switch				; switch to process ax (rest of rax = 0)
global switchForceInt			; switch without failure (try until possible)
global sysSwitch
global switchForceSys
global intK0
global intK2
global rtuspc				; return to userspace
global saveStateInt			; save the state after an interrupt
global saveStateSys
global sysRpcall
global sysRpret
extern strp
extern getPage
extern freePage
extern setPfFail
extern unsetPf
extern getPhysAddr

section .rodata

pidCr3:	dq 0xffffe00000000068
	; ss stands for saved state
ssRip:	dq 0xffffff0000013280
ssRflags:
	dq 0xffffff0000013288
ssPaI:	dq 0xffffff0000013003		; ss page getPhysicalAddress input
ssOSs:	dq 0xffffff0000013290		; ss old ss page
ssRsp:	dq 0xffffff0000013220
ssOORsp:dq 0xffffff0000013298		; ss old old rsp
ssPg:	dq 0xffffff0000013000
intHand:dq 0xffffff0000010000		; interrupt handlers

section .text

sysRpcall:
	cli				; avoid issues if an interrupt tried to switch away without an ability to switch back
	inc byte [0x2060]		; raise the blocking level of the process
	mov rax, [ssRip]
	mov [rax], rcx			; save rip
	add rax, 8
	mov [rax], r11			; save rflags
	mov ecx, [0x1000]		; get the current PID
	mov rax, [pidCr3]
	mov edx, edx
	shl rdx, 12
	add rax, rdx
	mov rax, [rax]			; get the cr3
	mov cr3, rax
	mov [1060], edx			; set the calling pid
	push rdi
	push rsi
	mov rsi, [ssPg]
	mov rdi, 0x1100
	mov rcx, 0x700 >> 3
	rep movsq			; save the old ss
	pop rdi
	pop rsi
	mov rax, [ssRsp]
	mov rsp, [rax]
	mov rcx, [0x2000]
	sysret

	; TODO TODO TODO TODO
rpcFail:
sysRpret:
	; this is rather basic, just go back in context and sysret
	cli				; just to be safe
;	pop qword [0xffffff00000132a8]	; restore the old stack
	call setPfFail
	pop rax				; the old rpcaller
	mov [0x1050], eax
	; let the current running process run again
	pop rax
	dec byte [0x2060]

intK2:	call saveStateInt
	mov eax, edx			; get the PID into the correct register and clear the upper bits (get just the PID)
	jmp switch

intK0:	push rax
	push rbx
	mov rax, [kspCore]
	mov [rax], rsp
	mov rax, 1
	mov rbx, 0
	cmp qword [rsp + 8], 0x8
	cmovne rax, rbx
	mov [0x1008], al
	pop rbx
	pop rax
	call saveStateInt
	add rsp, 40
	xor rax, rax
	jmp switchForce

sysSwitch:
saveStateSys:				; save state after a system call
	push rax
	mov rax, [ssRip]
	mov [rax], rcx			; saved rip
	add rax, 8
	mov [rax], r11			; saved rflags
	add rax, 0x10
	mov rcx, [rax]			; saved rsp
	sub rax, 0x78
	mov [rax], rsp
	; for cs and ss, there is only one valid starting segment for each
	mov cx, 0x1b
	add rax, 0x80
	mov [rax], cx			; "saved" cs
	mov cx, 0x23
	add rax, 2
	mov [rax], cx			; "saved" ss
	jmp savSt			; not poping rax was intentional

saveStateInt:				; save state after an interrupt
	push rax
	push rbx
	mov rbx, [rsp + 40]		; saved rsp
	mov rax, [ssRsp]
	mov [rax], rbx
	mov bx, [rsp + 48]		; saved ss
	add rax, 0x82
	mov [rax], bx
	mov rbx, [rsp + 16]		; saved rip
	sub rax, 0x22
	mov [rax], rbx
	mov rbx, [rsp + 24]		; saved cs
	add rax, 0x20
	mov [rax], rbx
	mov rbx, [rsp + 32]		; saved rflags
	sub rax, 0x18
	mov [rax], rbx
	pop rbx
savSt:	mov rax, [ssPg]
	fxsave [rax]
	add rax, 0x200
	push rbx
	mov rbx, [rsp + 8]
	mov [rax], rbx			; rax
	pop rbx
	add rax, 8
	mov [rax], rbx
	add rax, 8
	mov [rax], rcx
	add rax, 8
	mov [rax], rdx
	add rax, 8
	mov [rax], rbp
	add rax, 8
	mov [rax], rsi
	add rax, 8
	mov [rax], rdi
	add rax, 8
	mov [rax], r8
	add rax, 8
	mov [rax], r9
	add rax, 8
	mov [rax], r10
	add rax, 8
	mov [rax], r11
	add rax, 8
	mov [rax], r12
	add rax, 8
	mov [rax], r13
	add rax, 8
	mov [rax], r14
	add rax, 8
	mov [rax], r15
	add rax, 8
	pop rax
	ret

switchForceSys:
	; because we're a syscall we're on the per process stack
	; we need to switch over to core stack
	pop rcx
	mov [0x1080], rax
	mov rax, [ssOORsp]
	mov rsp, [rax]
	add rax, 0x10
	mov [rax], rsp
	sub rax, 0x10
	mov [rax], rcx			; save the syscall rsp
	mov rax, [kspCore]
	mov rsp, [rax]			; go to the core stack
	mov rax, [1080]
	jmp switchForce
switchForceInt:
	sub rsp, 56
	mov rbx, [kspCore]
	mov [rbx], rsp
	add rsp, 56
switchForce:
	shl eax, 1
	add rax, [intHand]
snflp0:	pause
	mov rbx, [rax]
	cmp rbx, 0
	je snflp0
	mov rax, cr3
	mov cr3, rbx			; load the table
snflp1:	cmp byte [0x2060], 0
	jne snflp1
	lock inc byte [0x2060]
	cmp byte [0x2060], 1
	jne snflp1
	jmp swbdy

switch:	shl eax, 1
	add rax, [intHand]
	mov rbx, [rax]
	cmp rbx, 0
	je fail
	mov rax, cr3
	mov cr3, rbx			; load the table
	cmp byte [0x2060], 0
	jne swf2
	lock inc byte [0x2060]
	cmp byte [0x2060], 1
	jne swf2
swbdy:	call strp
	mov rbx, [ssPg]
	fxrstor [rbx]
	mov ax, 0x0020
	push rax			; stack segment
	add rbx, 0x220
	mov rax, [rbx]
	push rax    			; rsp
	add rbx, 0x68
	mov rax, [rbx]
	push rax			; push rflags
	mov ax, 0x0018
	push rax			; code segment
	sub rbx, 8
	mov rax, [rbx]
	push rax			; rip
	sub rbx, 0x80
	mov rax, [rbx]
	push rax
	add rbx, 8
	mov rax, [rbx]
	push rax			; rbx
	add rbx, 8
	mov rcx, [rbx]
	add rbx, 8
	mov rdx, [rbx]
	add rbx, 8
	mov rbp, [rbx]
	add rbx, 8
	mov rsi, [rbx]
	add rbx, 8
	mov rdi, [rbx]
	add rbx, 8
	mov r8, [rbx]
	add rbx, 8
	mov r9, [rbx]
	add rbx, 8
	mov r10, [rbx]
	add rbx, 8
	mov r11, [rbx]
	add rbx, 8
	mov r12, [rbx]
	add rbx, 8
	mov r13, [rbx]
	add rbx, 8
	mov r14, [rbx]
	add rbx, 8
	mov r15, [rbx]
	xor eax, eax
	cmp byte [0x1008], al
	je stkern
	pop rbx
	pop rax
	iretq
stkern:	mov qword [rsp - 24], 0x8
	pop rbx
	pop rax
	cli
	add rsp, 40
	mov [0x1080], rax
	mov rax, [kspCore]
	mov [rax], rsp
	mov rax, [0x1080]
	sub rsp, 40
	iretq
swf2:	mov cr3, rax
;	jmp rtuspc			; TODO TODO TODO

fail:	stc
	iretq
