	;; fork.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; handles process creation
	;; int38
	;; it also provides a function to save on code size
	;; i.e. waitrnd (wait for a random amount of time)

	;; in order to fork, we do a two step process
	;; in the first step, we copy over all the PTEs and put in a new paging structure
	;; this step is only applied to the user pages
	;; in the second step, we page in all of the paging structures and set up the kernel reserved area and process-local areas

section .rodata
global kname
global genname				; the name of the generation of the kernel abi (i.e. "Batrson")

kname:	db "Greyhound!!", 0
genname:db "Batrson", 0

section .bss
global z4k

z4k:	resb 4096

section .text
global waitrnd
global int38
extern getpgk
extern incpgk
extern getpa
extern saveState
extern stisp

int38:	push rsi
	push rdi
	push r8
	push rcx
	sub rsp, 32
	call stisp
	add rsp, 32
	mov rsi, 0x7000			; increment the user's pages
	mov rdi, 0x7ffffffff000
	call increg
	mov rsi, 0xffff800000000000
	mov rdi, 0xfffffefffffff000
	call increg
	xor rax, rax
	mov al, 0x03
	int 36
	mov rsi, 0xffffff2400200000
	xor rdi, rdi
	mov rcx, 512
	rep movsq
	mov r8, [0xffffff8000000000]
	; r8 now holds the cr3 for the child
	; it points to page 0x0000 -- a PML4
	mov rcx, 510
loop0:	cmp qword [8 * rcx - 8], 0
	je cnt0
	call getpgk
	or al, 7
	mov [8 * rcx - 8], rax
	mov [0xffffff8000000018], rax	; get a page for the PDPT and put it at 0x3000 and in the PML4
	push rcx
	mov rcx, 512
loop1:	cmp qword [0x3000 + 8 * rcx - 8], 0
	je cnt1
	call getpgk
	or al, 7
	mov [0x3000 + 8 * rcx - 8], rax
	mov [0xffffff8000000020], rax	; get a page for the PD and put it at 0x4000 and in the PDPT
	push rcx
	mov rcx, 512
loop2:	cmp qword [0x4000 + 8 * rcx - 8], 0
	je cnt2
	call getpgk
	or al, 7
	mov [0x4000 + 8 * rcx - 8], rax
	mov [0xffffff8000000028], rax	; get a page for the PT and put it at 0x5000 and in the PD
	mov rsi, rcx
	dec rsi
	shl rsi, 12
	add rsi, 0xffffff8000000000
	mov rdi, 0x5000
	push rcx
	mov rcx, 512
	rep movsq			; copy the PT
	pop rcx
cnt2:	loop loop2
	pop rcx
cnt1:	loop loop1
	pop rcx
cnt0:	loop loop0
	; step one complete
	; we chose simplicity and efficiancy over pre-emptability here
	; we set up the higher paging and reserved kernel area all at once and first, which stops pre-emptability
	; note that our PML4 was copied over and that we can thus switch to it without setting up stuff
	; we must, however, disable interrupts to avoid getting the wrong cr3 set while working
	; at this point, we also set up the return for the parent
	; the parent's return is slightly more odd -- it is simply allowed to run and given the return value as if it were pre-empted
	mov rbx, r8
	pop rcx
	pop r8
	pop rdi
	pop rsi
	cli
	xor rax, rax
	mov ax, [0x1062]
	push rax
	mov rax, 0xffffff0000020000 - 8
loop5:	add rax, 8
	cmp qword [rax], 0
	jne loop5
	mov [rax], rbx
	call waitrnd
	cmp [rax], rbx
	jne loop5
	sub eax, 0x20000
	shl eax, 3
	mov [0x1062], ax
	call saveState			; here we give the parent its return value TODO: this symbol is not valid. Change when converting/updating
	cli
	mov cr3, rbx
	push rsi
	push rdi
	push rcx
	push rax
	; now we have done some minor set up stuff
	; on to step two
	call getpgk
	or al, 3
	mov [0xffffff8000000018], rax	; PDPT at 0x3000
	mov rsi, 0xffffff24001fe000
	mov rdi, 0x3000
	mov rcx, 512
	rep movsq
	mov [0xff0], rax		; PDPT into PML4
	call getpgk
	or al, 3
	mov [0xffffff8000000020], rax	; PD at 0x4000
	mov rsi, 0xffffff203fe00000
	mov rdi, 0x4000
	mov rcx, 512
	rep movsq
	mov [0x3000], rax		; PD into PDPT
	call getpgk
	or al, 3
	mov [0xffffff8000000028], rax	; PT at 0x5000
	mov rsi, 0xffffffff00000000
	mov rdi, 0x5000
	mov rcx, 512
	rep movsq
	mov [0x4000], rax		; PT into PD
	call getpgk
	mov rbx, 0x8000000000000003
	or rax, rbx
	mov [0xffffff80000000030], rax	; IO map lower at 0x6000
	mov rsi, 0xffffff0000011000
	mov rdi, 0x6000
	mov rcx, 512
	rep movsq
	mov [0x5088], rax		; IO map lower into PT
	call getpgk
	mov rbx, 0x8000000000000003
	or rax, rbx
	mov [0xffffff8000000030], rax	; IO map higher
	mov rsi, 0xffffff0000012000
	mov rdi, 0x6000
	mov rcx, 512
	rep movsq
	mov [0x5090], rax		; IO map higher into PT
	call getpgk
	mov rbx, 0x8000000000000003
	or rax, rbx
	mov [0x5098], rax		; saved process state directly into PT (not initialized by the old one)
	mov qword [0xffffff0000013000], 0
	; now we set up the higher paging
	; we start higher and then move down lower
	call getpgk
	or al, 3
	mov [0xffffff8000000020], rax	; PD for 0xffffff2400000000 at 0x4000
	mov [0x3048], rax
	call getpgk
	or al, 3
	mov [0xffffff8000000028], rax	; PT at 0x5000 for 0xffffff2400200000
	mov [0x4008], rax
	mov rax, cr3
	mov [0x5000], rax
	; PML4 done, on to the PDPTs
	call getpgk
	or al, 3
	mov [0xffffff8000000028], rax	; PT at 0x5000 for 0xffffff2400000000
	mov [0x4000], rax
	call getpgk
	mov rbx, 0x8000000000000005
	or rax, rbx
	mov [0xff8], rax		; what will be the high PDPT
	xor rsi, rsi
	mov rdi, 0x5000
	mov rcx, 512
	rep movsq			; copy over the PDPTs from the PML4
	; PDPTs done, on to the PDs
	call getpgk
	or al, 3
	mov [0xffffff8000000020], rax	; PD for 0xffffff2000000000 to 0xffffff203fffffff at 0x4000
	mov [0x3048], rax
	; at this point we must copy over the existant PDs
	mov rcx, 511
loop3:	cmp qword [8 * rcx - 8], 0
	je cnt3
	call getpgk
	or al, 3
	mov [0xffffff8000000028], rax	; PT at 0x5000
	mov rsi, rcx
	dec rsi
	shl rsi, 12
	mov rdi, 0x5000
	push rcx
	mov rcx, 512
	rep movsq
	pop rcx
cnt3:	loop loop3
	; PML4, PDPTs, and PDs are in place
	; now we must copy the PTs
	; we prepared a PDPT earlier
	mov qword [0xffffff8000000028], 0
	mov qword [0xffffff8000000030], 0
	; clear some pages to avoid confusing the parent
	mov rax, [0xff8]
	xor al, 6			; switch the mode to fault on user access and add RW
	mov [0xffffff8000000018], rax	; PDPT at 0x3000
	mov rcx, 512
loop4:	cmp qword [8 * rcx - 8], 0
	je cnt4
	call getpgk
	or al, 5
	mov [0x3000 + 8 * rcx - 8], rax
	xor al, 6
	mov [0xffffff8000000020], rax	; PD at 0x4000
	mov rsi, rcx
	shl rsi, 12
	add rsi, 0xffffff2400000000
	mov rdi, 0x4000
	push rcx
	mov rcx, 512
	rep movsq
	; by copying the PDPT as a PD, we save a few pages but get the same result
	; this is why we don't include the PDs for the PTs in the PD area -- they don't exist
	; this also means that the PTs are updated immedietly
	pop rcx
cnt4:	loop loop4
	; now we get new stuff for the user visible process specific stuff
	call getpgk
	mov rbx, 0x8000000000000005
	or rax, rbx
	pop rcx
	mov bx, [0x1000]		; parent PID
	mov di, [0x1062]		; next child
	mov [0x1062], cx		; child list
	mov [0xffffff8000000008], rax	; ROPDA
	mov [0x1000], cx
	mov [0x1064], di
	mov [0x1066], cx
	mov [0x1002], bx
	mov dword [0x1004], 0x100	; kernel ABI version
	mov byte [0x1009], 48		; bytes in virtual address space
	mov rsi, kname
	mov rdi, 0x104c
	mov rcx, 12
	rep movsb			; kernel name
	mov rsi, genname
	mov rdi, 0x1058
	mov rcx, 8
	rep movsb			; kernel generation name
	mov word [0x1062], 0
	call getpgk
	mov rbx, 0x8000000000000005
	or rax, rbx
	mov [0xffffff8000000000], rax
	mov rsi, 0x2000
	xor rdi, rdi
	mov rcx, 512
	rep movsq
	mov qword [0xffffff8000000000], 0
	dec byte [0x2060]
	mov [0xffffff8000000010], rax
	; all done, ready for return
	push rbx
	call strp
	pop rbx
	pop rcx
	pop rdi
	pop rsi
	pop rax
	mov [0x1064], ax
	mov ax, [0x1000]
	mov [0x1066], ax
	xor rax, rax
	call strisp
	iretq

increg:	mov rax, rsi
	call getpa
	call incpgk
	add rsi, 0x1000
	cmp rsi, rdi
	jl increg
	ret

waitrnd:push rbx
	push rcx
	test qword [seed], 0xb800000000000000
	pushf
	shl qword [seed], 1
	pop rbx
	and rbx, 2 << 2
	shl rbx, 63 - 2
	add [seed], rbx
	jnc wrlb0
	add qword [seed], 6
wrlb0:	xor ecx, ecx
	mov cx, [seed]
	mov rsi, 0xffffff0000010000
	mov rdi, rsi
	rep movsb
	pop rcx
	pop rbx
	ret
