	;; gdt.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; the global descriptor table
	;; also handles the "task state statement"

global gdt
global gdtSize
global tss
global tssSize
global isp0
global ist0

section .rodata
gdt:				; this is a sample gdt, copy into the core local gdt
gdtSize:dw tss - gdt - 3
	dq gdt + 2		; null segment 0x0
	dw 0			; kernel code segment 0x8
	dw 0
	db 0
	db 0x82
	db 0x20
	db 0
	dw 0			; kernel process local data (stack) segment 0x10
	dw 0
	db 0
	db 0xee
	db 0x20
	db 0
	dw 0			; userspace code segment 0x1b
	dw 0
	db 0
	db 0xe2
	db 0x20
	db 0
	dw 0			; userspace data segment 0x23
	dw 0
	dw 0
	db 0
	db 0xee
	db 0x20			; kernel core local data (stack) segment 0x28
	db 0
	dw 0x2000 - (tss - gdt)	; tss size (includes io privilage bitmap)
	dw 0x5000 + (tss - gdt)	; low 16 bits of tss address
	db 0x01			; next 8 bits of tss address
	db 0x89			; type (DO NOT CHANGE)
	db 0
	db 0x00			; next 8 bits of tss address
	dd 0xffffff00		; high 32 bits of tss address
	dd 0			; tss descriptor 0x30
	

tss:	dd 0
isp0:	dq 0xffffff0000023000	; rsp for a same process interrupt to CPL 0
isp1:	dq 0
isp2:	dq 0
	dq 0
ist0:	dq 0xffffff0000016000	; rsp for a process changing interrupt to CPL 0
ist1:	dq 0
ist2:	dq 0
ist3:	dq 0
ist4:	dq 0
ist5:	dq 0
ist6:	dq 0
ist7:	dq 0
	dw 0
	dw 0x1000 - (tss - gdt)	; io privilage map (equal to i/o privilage bitmap)
tssSize:dw $ - tss
