	;; core_info.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; information on cores

section .bss
global core0rp
global core1rp
global core0id
global core1id
core0rp:resw 1				; the process core0 is running
core1rp:resw 1
core0id:resb 1				; the apic id of core0
core1id:resb 1

section .text
global strp				; set the running process

	; TODO TODO TODO TODO TODO
strp:	push rbx			; set the running process to the current process
	push rax
	push rdx
	push rcx
	mov rcx, core0rp
	mov rdx, core1rp
;	mov al, [0xffffff00000a0023]
	cmp al, [core0id]
	cmove rbx, rcx
	pop rcx
	cmp al, [core1id]
	cmove rbx, rdx
	pop rdx
	mov ax, [0x1000]
	mov [rbx], ax
	pop rax
	pop rbx
	ret
