	;; adoption.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; handles int40
	;; this transfers parental ownership of a process to a new parent
	;; without knowledge of a proper technical term, I shall call this "adoption"

section .rodata

proclist:
	dq 0xffffe00000000000

section .text
global sysAdopt
global tscAdopt
extern scJump
extern scError
extern setPfFail
extern unsetPf

tscAdopt:
	push rcx
	mov cl, 1
	jmp adopt

sysAdopt:
	push rcx
	mov cl, 2
;	jmp adopt

adopt:	mov [0x100a], al			; no-go for exit (al isn't 0)
	call setPfFail
	mov eax, edi
	add rax, [proclist]
	add ax, 0x68
	mov rax, [rax]				; make sure the new parent exists
	mov esi, [0x1000]			; for later, make sure the process is the parent
	mov edx, edx
	shl rdx, 12
	add dx, 0x068				; index cr3
	mov rax, [proclist]
	add rdx, rcx				; rdx indexes the target process cr3
	mov rax, [rdx]
	mov rdx, cr3				; cr3 to go back to
	mov cr3, rax				; enter the target process
	cmp [0x1070], esi			; make sure we're the parent
	jne adopt.notParent			; if not, check for tsc
.go	mov [0x1070], edi			; do the thing
	mov cr3, rdx				; go back to the caller
	mov al, 0
	mov [0x100a], al			; allow exits
	pop rcx
	call unsetPf
	jmp scJump
.notParent:
	cmp cl, 2
	je adopt.go				; if we're a tsc, not being a parent means nothing
	; if not,
	mov cr3, rdx				; go back
	mov rax, 1
	mov [0x100a], ah			; allow exits
	pop rcx
	call unsetPf
	jmp scError
