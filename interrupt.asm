	;; interrupt.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; the IDT and helper functions

%include "segments.asm"

section .bss
intTrust:
	resd 256 - 32			; trusted interrupt handlers

section .rodata
global idtr
global sysSyncInt

	dd 0
	dw 0				; padding
idtr:	dw 0x1000			; size
idtloc	dq 0xffffff000001f000		; location

section .text
global idtEntry					; format IDT entry dh from rax as the jump point and dl as (bits 0-3, type, bit 4 0, 5-6 DPL, 7 1), di as the destination CS
extern scJump

idtEntry:					; for sysSyncInt: trashes only rax
	push rbx
	xor rbx, rbx
	mov bl, dh				; get rbx to index the IDT
	shl bx, 4
	add rbx, [idtloc]			; rbx is the IDT entry address
	mov [rbx], ax				; low part of offset
	shr rax, 16
	mov [rbx + 2], di			; CS
	mov byte [rbx + 4], 0			; set up only for non-ist interrupts
						; for ist, use a static interrupt
	mov [rbx + 5], dl			; type etc
	mov [rbx + 4], ax			; next 16 bits of offset
	shr rax, 16
	mov [rbx + 8], eax			; high 32 bits of offset
	pop rbx
	ret

sysSyncInt:
	mov rax, [0x2008]
	mov dx, 0x00ef				; #DE, DPL of 3, trap gate (DPL ant trap gate carry through)
	mov di, CS_USER				; carries through
	call idtEntry
	mov rax, [0x2010]
	mov dh, 0x03				; #BP
	call idtEntry
	mov rax, [0x2018]
	mov dh, 0x04				; OF
	call idtEntry
	mov rax, [0x2020]
	mov dh, 0x05				; #BR
	call idtEntry
	mov rax, [0x2028]
	mov dh, 0x06				; UD
	call idtEntry
	mov rax, [0x2030]
	mov dh, 0x0d				; #GP
	call idtEntry
	mov rax, [0x2038]
	mov dh, 0x0e				; #PF
	call idtEntry
	mov rax, [0x2040]
	mov dh, 0x10				; #MF
	call idtEntry
	mov rax, [0x2048]
	mov dh, 0x11				; #AC
	call idtEntry
	mov rax, [0x2050]
	mov dh, 0x13				; #XM
	call idtEntry
	mov rax, [0x2058]
	mov dh, 0x0c				; #SS
	call idtEntry
	jmp scJump
