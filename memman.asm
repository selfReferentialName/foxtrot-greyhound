	;; memman.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; the memory manager
	;; it also provides internally used memory managment functions
	;; i.e. deapgk, incpgk, getpgk, and getpa

%include "segments.asm"


section .bss
nextPg:	resq 1				; physical address of a free page
.lock:	resb 1				; spinlock for that

section .rodata
global pageCr3

maxMem:	dq 0xffffe00000000
pageCr3:dq 0xffffff00000132b0
memMode:dq 0x8000000000000fff
nmMode:	dq 0x7ffffffffffff000
memAddr:dq 0x000ffffffffff000
freePg:	dq 0xffffff0000014000
freePgP:dq (0xffffff0000014000 >> 9) & 0x7fffffffff + 0x8000000000
ptOffst:dq 0x8000000000

section .text
global freePage				; dealocates a page for the kernel
global incPage				; increments "  "    "   "    "
global getPage				; get and place a page at rax with mode bits -- does not check permissions
global freePage				; dealocate the page at rax in paging mode
global int38
global int38
global int39
global sysGetPage
global sysDropPage
global sysSharePage
global sysTakePage
global sysCow
global cow
extern stisp
extern strisp
extern scJump
extern pidCr3
extern setPfPage
extern setPfFail
extern unsetPf

sysCow:	call setPfFail
	push rcx
	push rax
	mov rax, [rsp]
	and rax, [memMode]		; save the mode bits
	and rax, [nmMode]		; get the true address
	push rax			; save the address
	mov rcx, 512
	mov rdi, [freePg]
	mov rsi, rax
	cli				; because we need to use the free page, this must be a syscall-atomic operation
	rep movsq			; copy the page
	mov rdi, [pageCr3]
	mov rax, [rdi]
	mov rcx, cr3			; current cr3
	mov cr3, rax			; enter paging mode
	mov rax, [rsp]
	call freePage			; safely get rid of the pre-cow page
	mov rdi, freePgP
	mov rax, [rdi]
	and rax, [nmMode]		; get the physical address of the free page
	or rax, [rsp + 8]		; get the PTE for the post-cow page
	pop rdx
	shr rdx, 9
	add rdx, [ptOffst]
	mov [rdx], rax			; write the PTE for the post-cow page
	mov rax, [freePg]
	call getPage			; get a new free page
	mov rdx, [freePgP]
	add rax, 1
	mov [rdx], rax
	sti				; no longer using the free page
	mov cr3, rcx			; return to normal cr3
	add rsp, 8			; trash the mode
	pop rax
	call unsetPf
	jmp scJump

sysSharePage:				; TODO TODO TODO TODO
	call setPfPage
	mov edi, edi			; clear the top 32 bits of rdi
	shl rdi, 12
	add rdi, [pidCr3]
	mov rdi, [rdi]			; get the cr3 for the other process
	push rcx
	push r11
	push rbx
	mov rcx, cr3			; save the sharer main cr3
	mov rax, [pageCr3]
	mov rax, [rax]
	mov cr3, rax			; enter sharer paging mode
	mov rax, rdx
	shl rax, 12			; canonicalize
	shr rax, 21			; get the index in the page tables
	and al, 0xf8			; clear the low mode bits
	mov rax, [rax]			; get the PTE
	mov rbx, rax			; rbx will be what we put in the sharee PT
	and rax, [memMode]		; get the mode bits
	mov r11, rax
	and rax, 0xfff			; get the low mode bits to and over rdx's mode
	or rax, [memAddr]		; don't and out the address (will also canonicalize rdx)
	and rsi, rax			; restrict low mode
	shr r11, 63
	shl r11, 63			; select only the NX bit
	or rsi, r11			; restrict execution
	mov cr3, rdi			; go to the other process
	mov rax, [pageCr3]
	mov rax, [rax]
	mov cr3, rax			; enter paging mode
	mov rdx, rsi
	shl rdx, 1			; ignore NX bit as address
	shr rdx, 13			; clear mode bits & git the index/8
	mov [8 * rdx], rsi	

sysDropPage:
	call setPfFail
	mov rax, rdx
	shr rax, 12			; get rax into a state for easy restriction
	sub rax, 8
	add rax, 8			; restrict the low pages
	mov rdi, [maxMem]
	add rax, rdi
	sub rax, rdi			; restrict excessivley high pages
	shl rax, 12
	mov rsi, rax
	xor rax, rax
	push rcx
	mov rcx, 512
	rep stosq			; clear and check the page for existance
	pop rcx
	mov rax, rsi
	mov rdx, [pageCr3]
	mov rdx, [rdx]
	mov rdi, cr3
	mov cr3, rdx			; switch to paging mode
	call freePage
	mov cr3, rdi			; switch back
	call unsetPf
	jmp scJump

sysGetPage:
	call setPfPage
	mov rax, rdx
	shr rax, 12			; get rax into a state for easy restriction
	sub rax, 8
	add rax, 8			; restrict the low pages
	mov rdi, [maxMem]
	add rax, rdi
	sub rax, rdi			; restrict excessivley high pages
	shl rax, 12
	mov rdx, [pageCr3]
	mov rdx, [rdx]
	mov rdi, cr3
	mov cr3, rdx			; switch to paging mode
	call getPage
	mov rsi, rdx
	and rsi, [memMode]
	and rdx, [nmMode]
	shr rdx, 9
	or rsi, rax
	add rdx, [ptOffst]
	mov [rdx], rsi
	mov cr3, rdi			; switch back
	call unsetPf
	jmp scJump

freePage:
	push rax
	shl rax, 12
	shr rax, 24			; simplify canonical adresses and index memory map
	lock dec byte [rax + 0xffffff1000000000]
	jz freePage.done		; count down the link number in the memory map and only add it to the free page list if needed
	and ax, 0xf000			; page align rax
	push rbx
	mov bl, 1
.spin:	pause
	lock inc byte [nextPg.lock]
	cmp [nextPg.lock], bl
	je freePage.acq
	lock dec byte [nextPg.lock]
	jmp freePage.spin		; spinlock nextPg so two processors don't try to use it in different processes
.acq:	mov rbx, [nextPg]
	shl rax, 12
	mov [rax], rbx			; set the next page to get the page we're freeing
	mov bl, 0
	mov [nextPg.lock], bl		; release the lock
.done:	shr rax, 9
	add rax, [ptOffst]
	xor rbx, rbx
	mov [rax], rbx
	ret

getPage:push rax
	shl rax, 12
	shr rax, 12			; simplify canonical adresses
	push rbx
	xor rbx, rbx
	mov bl, 1
	shl rbx, 63
	and rbx, [rsp]
	or rax, rbx			; preserve NX bit
	add rsp, 8
	mov bl, 1
	mfence
.spin:	pause
	lock inc byte [nextPg.lock]
	cmp [nextPg.lock], bl
	je getPage.acq
	lock dec byte [nextPg.lock]
	jmp getPage.spin		; spinlock nextPg so two processors don't try to use it in different processes
.acq:	mov rbx, rax
	and rbx, [memMode]		; get just the mode bits
	shr rax, 9
	and al, 0xf8			; get an index in the page tables
	or rbx, [nextPg]		; set rbx to the page to put in
	mov [rax], rbx			; put in the page
	mov rbx, [nextPg]
	push rbx
	mov rbx, [rbx]
	mov [nextPg], rbx		; get the next nextPg
	mov bl, 0
	mfence
	mov [nextPg.lock], bl		; release the lock
	pop rax
	pop rbx
	add rsp, 8
;	jmp incPage

incPage:push rbx
	push rax
	mov rbx, 0x0000fffffffff000
	and rax, rbx
	shr rax, 12
	lock inc byte [rax + 0xffffff1000000000]
	pop rax
	pop rbx
	ret
