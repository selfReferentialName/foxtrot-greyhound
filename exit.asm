	;; exit.asm

	;; Copyright (C) 2017, 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; handles int39
	;; that is, removing a process from existance
	;; to save on code size, it also provides other functions
	;; i.e. ststr (wait for a certain relativley long (about a microsecond) amount of time)
	;; and ifence (serialize the instruction stream without trashing registers)

section .text
global int39
extern freePage
extern strp
global ststr				; stop to smell the roses
global ifence				; serialize the instructions
extern core0rp
extern core0id
extern core1rp
extern core1id
extern stisp
extern strisp
extern getpa
extern saveStateSys
extern switchForceInt

sysExit:; TODO: not fully converted
	; TODO: actually, might just scrap it
	push rcx
	call saveStateSys
	pop rcx
	cli
	cmp ax, [0x1000]
	je cnt0
	call ststr			; there is a race condition: if we try to have both the parent and child exit the child, the pages get set to 255 uses
	; check to make sure that we are the parent
	mov bx, [0x1000]
	push ax
	xor eax, eax
	pop ax
	mov rax, [8 * rax + 0xffffff0000000000]
	push rbx
	mov rbx, cr3
	mov cr3, rax
	mov rax, rbx
	pop rbx
	cmp bx, [0x1002]
	mov rax, cr3
	jne fail0
	add rsp, 40
	jmp cnt3
cnt0	push ax
	xor eax, eax
	pop ax
	mov rbx, [8 * rax + 0xffffff0000010000]
	cmp rbx, 0
	je fail0			; at this point we have almost commited, and have no time to waste to avoid race conditions
	mov dx, [0x1000]
	mov rax, cr3
	mov cr3, rbx
	cmp dx, [0x1002]
	jne fail1
	mov [0x1000], dx
	inc byte [0x2060]
	call strp
i39go:	add rsp, 40			; we are go for process removal, notify other processes and prepare for phase one
	not word [0x1000]		; set 0x1000 to something other than the PID to cause a failure in a child attempting to exit
	mov eax, 0x00004020		; icr to pre-empt a process
	push rax
	push rdx
	mov eax, 0x1
	cpuid
	pop rdx
	pop rax
	shr ebx, 28
	mov bl, [0x1067]
	mov si, [0x1000]
	xor cl, cl
	cmp [core0id], al
	je cnt1
	mov ch, [core0id]
	cmp [core0rp], si
	cmove rsi, [0x1058]
	jmp cnt2
cnt1:	mov ch, [core1id]
	cmp [core1rp], si
	cmove rsi, [0x1058]
cnt2:	cmp rsi, 0
	je cnt3				; we do not need to interrupt any processor
	xor ebx, ebx			; here, we actually do
	mov bl, ch
	shl rbx, 56
	or rax, rbx
	mov rbx, rax
	shr rbx, 32
	mov [0xffffff000001a300], eax
	mov [0xffffff000001a310], ebx
cnt3:	add rsp, 40
	call stisp
	xor r8, r8
	mov r8w, [0x1000]
	xor r9, r9
	mov r9w, [0x1002]
	xor r10, r10
	mov r10w, [0x1062]
	cli
	mov rax, intnul			; clear out the #PF exeption to do nothing
	mov [0xffffff00000100e0], ax
	shr rax, 16
	mov word [0xffffff00000100e2], 0x08
	mov word [0xffffff00000100e4], 0x8f01
	mov [0xffffff00000100e6], ax
	shr rax, 16
	mov [0xffffff00000100e8], eax
	mov dword [0xffffff00000100ec], 0
	; now we are ready
	; this is the time to remove the pages
	; no turning back
	; in the first step, we remove the user's pages
	mov rsi, 0xffffff8000000000
	mov rcx, 510 * (1 << (48 - 12 - 9))
loop0:	lodsq
	call getpa
	call freePage
	loop loop0
	; in the second step, we remove the process specific kernel pages
	mov rax, 0xffffff0000011000
	call getpa
	call freePage
	mov rax, 0xffffff0000012000
	call getpa
	call freePage
	mov rax, 0xffffff0000013000
	call getpa
	call freePage
	; in the third step, we remove the higher paging
	mov rsi, 0xffffff2000000000
	mov rcx, 510 * (1 << (48 - 12 - 9)) / 4096
loop1:	lodsq
	call getpa
	call freePage
	loop loop1
	mov rsi, 0xffffff2400000000
	mov rcx, 510
loop2:	lodsq
	call getpa
	call freePage
	loop loop2
	; in the fourth step, we remove the final pages
	; first we load all the stuff we need to delete into the regesters
	; we cannot use memory because then we would have to remove a PML4 that is in use
	mov rax, 0xffffff203fc00000
	call getpa
	mov r10, rax			; low highish PD
	mov rax, 0xffffff24001fe000
	call getpa
	mov r11, rax			; highish PDPT
	mov rax, 0xffffff2400100000
	call getpa
	mov r12, rax			; PML4
	mov rax, 0xffffff0000016000
	call getpa
	mov cr3, rax
	mov rax, r10
	call freePage
	mov rax, r11
	call freePage
	; all pages of the process removed, time to clean up and go to the parent
	mov qword [8 * r8 + 0xffffff0000020000], 0	; remove the process's entry
	; now we give the parent the children
	mov rax, [8 * r9 + 0xffffff0000020000]
	mov cr3, rax
	mov r11w, [0x1062]
	mov [0x1062], r10w
loop3:	mov rax, [8 * r10 + 0xffffff0000020000]
	mov cr3, rax
	mov [0x1002], r9w
	mov r10w, [0x1064]
	cmp r10w, [0x1000]
	jne loop3
	mov [0x1064], r11w
	mov rax, [8 * r11 + 0xffffff0000020000]
	mov cr3, rax
	mov [0x1066], r10w
	; all done, go to the parent
	mov rax, r9
	jmp switchForceInt

ststr:	push rsi
	push rdi
	push rcx
	mov rsi, 0x1000
	mov rdi, 0x1000
	mov ecx, 1 << 13
	rep movsb
	pop rcx
	pop rdi
	pop rsi
	ret

intnul:	cmp word [rsp + 8], 0x8
	je irt
	add qword [rsp], 2
	xor rax, rax
irt:	iretq

fail0:	xor rax, rax
	jmp switchForce

fail1:	mov cr3, rax
	call strisp
	iretq
