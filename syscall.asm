	;; syscall.asm

	;; Copyright (C) 2018 J Rain De Jager

	;; This program is free software: you can redistribute it and/or modify
	;; it under the terms of the GNU General Public License as published by
	;; the Free Software Foundation, either version 3 of the License, or
	;; (at your option) any later version.

	;; This program is distributed in the hope that it will be useful,
	;; but WITHOUT ANY WARRANTY; without even the implied warranty of
	;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	;; GNU General Public License for more details.

	;; You should have received a copy of the GNU General Public License
	;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

	;; translate the landing from a syscall into calling the internal syscall handler
	;; TODO: test to make sure the calling process can use a trusted syscall
	;; NB: syscall handlers need to jmp to scJump in order to return from a syscall

SYS_SYNCINT	equ 0x00			; copy the interrupts from the user rw data page to the idt
SYS_SWITCH	equ 0x01			; switch to a different process
SYS_RPC		equ 0x02			; rpcall
SYS_RPRET	equ 0x03			; return from rpcall
SYS_GETPG	equ 0x04			; get a page
SYS_DROPPG	equ 0x05			; relinquish access to a page
SYS_SHAREPG	equ 0x06			; share a page
SYS_CLONE	equ 0x07			; fork without cow
SYS_FORK	equ 0x08			; fork with cow
SYS_KILL	equ 0x09			; kill a child/exit
SYS_ADOPT	equ 0x0a			; transfer parentship of a child
SYS_TAKEPG	equ 0x0b			; accept a SYS_SHAREPG call
SYS_SHAREIO	equ 0x0c			; share access to an i/o port
SYS_DROPIO	equ 0x0d			; drop access to a specific port
SYS_COW		equ 0x0e			; copy a COW page
SYS_IO		equ 0x0f			; run an IO batch
TSC_SHARETSC	equ 0x10			; share access to a tsc
TSC_SETSCHED	equ 0x11			; set up a temporary scheduler
TSC_APICW	equ 0x12			; write to an APIC register
TSC_APICR	equ 0x13			; read from an APIC register
TSC_EXIT	equ 0x14			; kill an arbitrary process
TSC_GIVEINT	equ 0x15			; shift handling of an interrupt
TSC_DROPTSC	equ 0x16			; drop own access to a tsc
TSC_ADOPT	equ 0x17			; transfer ownership of an arbitrary process
TSC_RDPTE	equ 0x18			; read pte that contains rdx
TSC_GETPGSEQ	equ 0x19			; get physically sequential pages
SYS_EXIT	equ 0x20

section .bss
extern intRsp

tmp:	resq 1

section .rodata

ssRsp:	dq 0xffffff0000013220
ssORsp:	dq 0xffffff00000132a8
procCr3:dq 0xffffe00000000068

section .text
global scLand					; what syscall jumps to
global scJump					; return from a syscall
global stisp
global scError					; inform the process of a syscall error (rax is the error code)
global tscShareTsc
extern sysSwitch
extern sysSyncInt
extern sysRpcall
extern sysRpret
extern sysGetPage
extern sysDropPage
extern sysSharePage
extern sysClone
extern sysFork
extern sysExit
extern sysAdopt
extern sysTakePage
extern sysShareIo
extern sysDropIo
extern sysIo
extern sysCow
extern tscSetSched
extern tscApicW
extern tscApicR
extern tscExit
extern tscGiveInt
extern tscDropTsc
extern tscAdopt
extern tscReadPte
extern tscGetPgSeq
extern setPfFail

scLand:	cli					; switch stacks
	mov [0x1080], rax
	mov rax, [ssRsp]
	mov rsp, [ssRsp]
	add rax, 0x78
	mov [rax], rsp
	add rax, 0x10
	push qword [rax]
	mov [rax], rsp
	mov rax, [0x1080]
	sti					; only to disable interrupts
	test al, 0x10				; check for tsc or sys
	jnz tscLand
sysLand:cmp al, SYS_SWITCH
	je sysSwitch
	cmp al, SYS_RPC
	je sysRpcall
	cmp al, SYS_RPRET
	je sysRpret
	cmp al, SYS_COW
	je sysCow
	cmp al, SYS_GETPG
	je sysGetPage
	cmp al, SYS_IO
	je sysIo
	cmp al, SYS_SYNCINT
	je sysSyncInt
	cmp al, SYS_DROPPG
	je sysDropPage
	cmp al, SYS_SHAREPG
	je sysSharePage
;	cmp al, SYS_CLONE
;	je sysClone
;	cmp al, SYS_FORK
;	je sysFork
	cmp al, SYS_ADOPT
	je sysAdopt
;	cmp al, SYS_TAKEPG
;	je sysTakePage
	cmp al, SYS_SHAREIO
	je sysShareIo
	cmp al, SYS_DROPIO
	je sysDropIo
;	cmp al, SYS_EXIT
;	je sysExit
scBad:	xor rax, rax				; erroneous syscall (does not exist)
scError:cli
	mov rax, [ssORsp]
	pop qword [ssORsp]
	sub rax, 0x10
	mov [rax], rsp				; save the process interrupt rsp
	sub rax, 0x70
	mov rsp, [rax]				; rsp is the processes rsp, because we're fake interrupting it
	mov dx, 0x20				; userspace stack segment
	push rdx
	mov rdx, rsp
	add rdx, 8				; pre "interrupt" rsp
	push rdx
	mov dx, 0x18				; userspace code segment
	push rdx
	mov rdx, [0x2080]			; syscall error jump page
	push rdx
	push rax				; error code
	sysret

tscLand:push rcx
	push rdx
	mov dx, 1
	sub al, 0x10
	mov cl, al
	shl dx, cl				; get cx to the bit mask for checking privilage
	add al, 0x10
	mov si, [0x1010]			; get si to the privilage bitmap
	test dx, si
	pop rdx
	jz tscLand.noPermit			; no permissions
	pop rcx
	cmp al, TSC_SHARETSC
	je tscShareTsc
;	cmp al, TSC_SETSCHED
;	je tscSetSched
;	cmp al, TSC_APICW
;	je tscApicW
;	cmp al, TSC_APICR
;	je tscApicR
;	cmp al, TSC_EXIT
;	je tscExit
	cmp al, TSC_ADOPT
	je tscAdopt
;	cmp al, TSC_RDPTE
;	je tscReadPte
;	cmp al, TSC_GETPGSEQ
;	je tscGetPgSeq
	jmp scBad
.noPermit:
	mov rax, 6				; tsc not permitted
	jmp scError

tscShareTsc:
	push rcx
	sub ah, 0x10
	mov cl, ah
	shl di, cl				; get di to the bit mask for checking privilage (we'll use it later)
	pop rcx
	mov si, [0x1010]			; get si to the privilage bitmap
	test di, si
;	jz tscShareTsc.error			; no permissions TODO TODO TODO actually throw errors over permissions
	call setPfFail
	mov edx, edx
	shl rdx, 12
	add rdx, [procCr3]
	mov rax, [rdx]
	cli
	mov rsi, cr3				; save cr3
	mov cr3, rax				; switch processes
	or [0x1010], di				; do the thing
	mov cr3, rsi				; switch back
	sti
;	jmp scJump

scJump:	mov [0x1080], rax
	mov rax, [ssORsp]
	pop qword [rax]
	sub rax, 0x80
	mov rsp, [rax]
	sysret
