AS=nasm
ASFLAGS=-felf64
LD=ld.lld
LDFLAGS=
OBJFILES=adoption.o context.o core_info.o gdt.o interrupt.o io.o kpagefault.o memman.o syscall.o

greyhound: greyhound.elf
	objcopy -O binary greyhound.elf greyhound 

greyhound.elf: $(OBJFILES) link.ld
	$(LD) $(LDFLAGS) -o greyhound.elf -T link.ld $(OBJFILES)

adoption.o: adoption.asm
	$(AS) $(ASFLAGS) adoption.asm

context.o: context.asm
	$(AS) $(ASFLAGS) context.asm

core_info.o: core_info.asm
	$(AS) $(ASFLAGS) core_info.asm

exit.o: exit.asm
	$(AS) $(ASFLAGS) exit.asm

fork.o: fork.asm
	$(AS) $(ASFLAGS) fork.asm

gdt.o: gdt.asm
	$(AS) $(ASFLAGS) gdt.asm

interrupt.o: interrupt.asm
	$(AS) $(ASFLAGS) interrupt.asm

io.o: io.asm
	$(AS) $(ASFLAGS) io.asm

kpagefault.o: kpagefault.asm
	$(AS) $(ASFLAGS) kpagefault.asm

memman.o: memman.asm
	$(AS) $(ASFLAGS) memman.asm

syscall.o: syscall.asm
	$(AS) $(ASFLAGS) syscall.asm
